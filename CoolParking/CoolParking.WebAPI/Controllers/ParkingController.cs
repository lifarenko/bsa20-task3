﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api")]
    [ApiController]
    public class ParkingController : ControllerBase
    {

        private IParkingService parkingService;
        public ParkingController(IParkingService service)
        {
            parkingService = service;
        }



        // GET api/parking
        [HttpGet("parking/balance")]
        public async Task<ActionResult<decimal>> GetBalance()
        {
            
            return Ok(await Task.Run(() => parkingService.GetBalance()));
        }


        [HttpGet("parking/capacity")]
        public async Task<ActionResult<int>> GetCapacity()
        {
            
            return Ok(await Task.Run(() => parkingService.GetCapacity()));
        }



        [HttpGet("parking/freePlaces")]
        public async Task<ActionResult<int>> GetFreePlaces()
        {
            
            return Ok(await Task.Run(() => parkingService.GetFreePlaces()));
        }



        [HttpGet("transactions/last")]
        public async Task<ActionResult<TransactionInfo>> GetLastTransaction()
        {
            TransactionInfo[] transactionInfos = parkingService.GetLastParkingTransactions();
            if(transactionInfos.Length==0)
                return Ok();
            else
            return Ok(transactionInfos[^1]);
        }


        [HttpGet("transactions/all")]
        public async Task<ActionResult<string>> GetAllTransictions()
        {
            try
            {
                string t="\"";
                 t += parkingService.ReadFromLog();
                t+="\"";
                return Ok(t);
            }
            catch(InvalidOperationException)
            {
                return NotFound(new { message = "File doesn't exist" });
            }
        }




        [HttpGet("vehicles/{id}")]

        public async Task<ActionResult<Vehicle>> GetId(string id)
        {
            string pattern = @"[A-Z]{2}-\d{4}-[A-Z]{2}";

            if (Regex.IsMatch(id, pattern))
            {
                try
                {
                    Vehicle v = parkingService.GetVehicle(id);
                    return Ok(v);

                }
                catch (ArgumentException)
                {
                    return NotFound(new { message = "Vehicle not found" });
                }
            }
            else
            {
                return BadRequest(new { message = "Id is invalid" });
            }


        }

        [HttpGet("vehicles")]

        public async Task<ActionResult<List<Vehicle>>> GetVechicles()
        {
            return Ok(await Task.Run(() => parkingService.GetVehicles()));
        }



        [HttpPost("vehicles")]

        public async Task<ActionResult> CreateAsync(object json)
        {          
            try
            {
                Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(json.ToString());
                
                if ((int)vehicle.VehicleType > 3|| (int)vehicle.VehicleType<0)
                    throw new Exception();
                    
                  string pattern = @"[A-Z]{2}-\d{4}-[A-Z]{2}";

                if (!Regex.IsMatch(vehicle.Id, pattern) || vehicle.Balance <= 0)
                {
                    throw new Exception();
                }   
                
                 parkingService.AddVehicle(vehicle);

                return CreatedAtAction(nameof(GetId), new { id = vehicle.Id }, vehicle);
            }
             catch(ArgumentException)
            {
                return BadRequest(new { message = "It is already existed" });
            }
            catch (JsonReaderException)
            {
                return BadRequest(new { message = "Invalid" });
            }
            catch (JsonSerializationException)
            {
                return BadRequest(new { message = "Invalid" });
            }
            catch (NullReferenceException)
            {
                return NotFound(new { message = "Empty request" });
            }
            catch (Exception)
            {
                return BadRequest(new { message = "Wrong" });
            }


        }


        [HttpDelete("vehicles/{id}")]

        public async Task<ActionResult> Delete(string id)
        {
            string pattern = @"[A-Z]{2}-\d{4}-[A-Z]{2}";

            if (Regex.IsMatch(id, pattern))
            {
                try
                {
                    parkingService.RemoveVehicle(id); 
                    return Ok(null);                   

                }
                catch (ArgumentException)
                {
                    return NotFound(new { message = "Vehicle not found" });
                }
            }
            else
            {
                return BadRequest(new { message = "Id is invalid" });
            }
           
        }

        public class ToUp
        {
            public string id { get; set; }
            public decimal Sum { get; set; }

        }


        [HttpPut("transactions/topUpVehicle")]

        public async Task<ActionResult<Vehicle>> Put(object ob)
        {
            try
            {
                if(ob==null)
                {
                    throw new NullReferenceException();
                }
                ToUp toUp = JsonConvert.DeserializeObject<ToUp>(ob.ToString());
                parkingService.TopUpVehicle(toUp.id, toUp.Sum);
                return Ok(await Task.Run(() => parkingService.GetVehicle(toUp.id)));
            }
            catch (JsonReaderException)
            {
                return BadRequest(new { message = "Invalid" });
            }
             catch(JsonSerializationException)
            {
                return BadRequest(new { message = "Invalid" });
            }
            catch(NullReferenceException)
            {
                return NotFound(new { message = "Empty request" });
            }
            catch (ArgumentException)
            {
                return NotFound(new { message = "Such vehicle doesn't exist" });
            }
            catch(Exception)
            {
                return BadRequest(new { message = "Wrong" });
            }


        }


    }
}
