﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static readonly decimal startBalance = 0;
        public static readonly int capacity = 10;
        public static readonly double coefficient = 2.5;
        public static readonly int withdrawTime = 5000;
        public static readonly int logTime = 60000;
        public static readonly Dictionary<VehicleType, decimal> tariff = new Dictionary<VehicleType, decimal>
        {
            {VehicleType.PassengerCar, 2 },
            {VehicleType.Truck, 5 },
            {VehicleType.Bus, 3.5m },
            {VehicleType.Motorcycle, 1 }
        };
             

    }
}