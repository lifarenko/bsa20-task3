﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using Fare;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public VehicleType VehicleType { get; set; }

        public decimal Balance { get; set; }

        public string Id { get; set; }

        public Vehicle(string Id, VehicleType VehicleType, decimal Balance)
        {
            
            this.VehicleType = VehicleType;
           
            string pattern = @"[A-Z]{2}-\d{4}-[A-Z]{2}";

            if (Regex.IsMatch(Id, pattern) && Balance>=0 )
            {
                this.Id = Id;
                this.Balance = Balance;
            }
            else
            {
                throw new ArgumentException();
            }

        }

        public Vehicle()
        {

        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            string pattern = @"[A-Z]{2}-\d{4}-[A-Z]{2}";
            var xeger = new Xeger(pattern);
            string str = xeger.Generate();

            return str;
        }



    }
}