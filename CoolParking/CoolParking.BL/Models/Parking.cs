﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance;
        
        public static decimal Balance { get; set; }

        public List<Vehicle> vehicles;


        private Parking()
        {
            vehicles = new List<Vehicle>();
        }

        public static Parking getInstance()
        {
            if (instance == null)
                instance = new Parking();
            Balance = Settings.startBalance;
            return instance;
        }
    }
}