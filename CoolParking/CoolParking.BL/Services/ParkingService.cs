﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.


using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Timers;

namespace CoolParking.BL.Models
{

    public class ParkingService : IParkingService, IDisposable
    {


        public Parking Parking  = Parking.getInstance();
        private readonly ILogService logService;
        private readonly ITimerService withdrawTimer;
        private readonly ITimerService logTimer;
        private List<TransactionInfo> transactions;


        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            
            transactions = new List<TransactionInfo>();

            this.logService = logService;
            this.logTimer = logTimer;
            this.withdrawTimer = withdrawTimer;
            this.logTimer.Interval = Settings.logTime;
            this.withdrawTimer.Interval = Settings.withdrawTime;
            logTimer.Elapsed += new ElapsedEventHandler(logTimer_Elapsed);
            withdrawTimer.Elapsed += new ElapsedEventHandler(withdrawTimer_Elapsed);
            logTimer.Start();           
            withdrawTimer.Start();

        }

        //public ParkingService()
        //{
        //    transactions = new List<TransactionInfo>();
        //}


        private void logTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            logService.Write("");
            
                foreach (var item in transactions)
                {
                    logService.Write(item.time.ToString("MM/dd/yyyy hh:mm:ss tt") + " " + item.Sum + " money withdrawn from vehicle with Id='" + item.Id+"' ");
                }
                transactions.Clear();
            
        }


        private void withdrawTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            decimal sum;
            
            foreach (var item in Parking.vehicles)
            {
                if(item.Balance>=Settings.tariff[item.VehicleType])
                {
                     sum= Settings.tariff[item.VehicleType];
                }
                else
                {
                    sum = Settings.tariff[item.VehicleType] * 2.5m;
                }
                item.Balance -= sum;
                TransactionInfo transactionInfo = new TransactionInfo()
                {
                    Id = item.Id,
                    Sum = sum,
                    time = DateTime.Now

                };

                transactions.Add(transactionInfo);
                Parking.Balance += sum;
            }
        }


        private int FindVechicle(string vehicleId)
        {
            return Parking.vehicles.FindIndex(item => item.Id == vehicleId);
        }

        public Vehicle GetVehicle(string id)
        {
            if (FindVechicle(id) != -1)
            { 
                return Parking.vehicles[FindVechicle(id)];
               
            }
            else throw new ArgumentException();
               
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() <= 0)
            {
                throw new InvalidOperationException();
            }
            else
            {
                if (FindVechicle(vehicle.Id) != -1)
                {
                    throw new ArgumentException();
                }
                else 
                Parking.vehicles.Add(vehicle);
            }
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        //public async Task<decimal> GetBalanceH()
        //{
            
        //    return await Parking.Balance;
        //}

        public decimal GetEarnedMoney()
        {
            decimal sum = 0;
            foreach (var item in transactions)
            {
                sum += item.Sum;
            }
            return sum;
        }


        public int GetCapacity()
        {
            return Settings.capacity;
        }

        public int GetFreePlaces()
        {
            return Settings.capacity - Parking.vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> vehicles = new ReadOnlyCollection<Vehicle>(Parking.vehicles);
            return vehicles;
        }


        public string ReadFromLog()
        {
            string str = logService.Read();
            return str;
        }

        public void RemoveVehicle(string vehicleId)
        {
            int index = FindVechicle(vehicleId);
            if (index == -1)
            {
                throw new ArgumentException();
            }
            else if (Parking.vehicles[index].Balance < 0)
            {
                throw new InvalidOperationException();
            }
            else
            {
                Parking.vehicles.RemoveAt(index);
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            int index = FindVechicle(vehicleId);
            if (index != -1 && sum >= 0)
            {

                Parking.vehicles[index].Balance += sum;

            }
            else
            {
                throw new ArgumentException();
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Parking.vehicles.Clear();
                    transactions.Clear();
                    Parking.Balance = 0;
                }
                if (File.Exists(logService.LogPath))
                {
                    File.Delete(logService.LogPath);
                    
                }
                //logTimer.Stop();
                //withdrawTimer.Stop();

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~ParkingService()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        //TODO: uncomment the following line if the finalizer is overridden above.
             GC.SuppressFinalize(this);
        }
        #endregion


    }
}