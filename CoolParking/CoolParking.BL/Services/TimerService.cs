﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Models
{
    public class TimerService : ITimerService
    {
        public double Interval { get ; set ; }
        private Timer timer=new Timer();

        public event ElapsedEventHandler Elapsed
        {
            add { this.timer.Elapsed += value; }
            remove { this.timer.Elapsed -= value; }
        }

        public void Dispose()
        {
            this.timer.Dispose();
        }

        public void Start()
        {
            timer.Interval = Interval;
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }







    }
}