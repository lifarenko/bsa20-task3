﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;


namespace CoolParking.CI
{
    class Program
    {


        private class MenuItem
        {
            public string Description { get; set; }
            public Func<Task> Execute { get; set; }
        }


        private static List<MenuItem> MenuItems;

        private static void PopulateMenuItems()
        {

            MenuItems = new List<MenuItem>
            {
              new MenuItem {Description = "Show Parking balance", Execute = ShowBalance},
              new MenuItem {Description = "Show capacity", Execute = Capacity},
              new MenuItem {Description = "Show free places", Execute = FreePlaces},
              new MenuItem {Description = "Show Transactions", Execute = ShowTransaction},
              new MenuItem {Description = "Show history Transaction", Execute = ShowHistory},
              new MenuItem {Description = "Show vechicles", Execute = ShowVechicles},
              new MenuItem {Description="Show single vehicle", Execute=ShowSingleVechicle},
              new MenuItem {Description = "Add vechicle", Execute = AddVechicle},
              new MenuItem {Description = "Remove vechicle", Execute = RemoveVechicle},
              new MenuItem {Description = "To up vechicle", Execute = ToUpVechicle},
              new MenuItem {Description="To end", Execute=End},
            };
        }

        private static async Task End()
        {

        }

        private static async Task RemoveVechicle()
        {
                       
            Console.WriteLine("Delete vehicle");
            Console.WriteLine("Input id");
            string id = Console.ReadLine();
            string s = $"vehicles/{id}";
            try
            {
                DeleteAsyncV(s);                
            }
            catch (ApiException api)
            {
                Console.WriteLine(api.StatusCode + " " + api.Content);
            }
            Console.ReadLine();
            
        }


        public class ToUp
        {
            public string id { get; set; }
            public decimal Sum { get; set; }

        }

        private static async Task ToUpVechicle()
        {
            Console.WriteLine("To up vehicle");
            Console.WriteLine("Please enter Id");
            string id = Console.ReadLine();

            Console.WriteLine("Please enter sum");
            int sum = Int32.Parse(Console.ReadLine());
            ToUp toUp = new ToUp() { id = id, Sum = sum };
            try
            {
                var vehicle = await PutAsyncV(toUp);
                Console.WriteLine(vehicle.Id + " " + vehicle.VehicleType + " " + vehicle.Balance);
            }
            catch(ApiException api)
            {
                Console.WriteLine(api.StatusCode + " " + api.Content);
            }
            Console.ReadLine();


        }


        private static async Task ShowSingleVechicle()
        {
            Console.WriteLine("Single vehicle");
            Console.WriteLine("Input id");
            string id = Console.ReadLine();
            string s = $"vehicles/{id}";
            try
            {
                var vehicle = await Get<Vehicle>.GetSimple(s);
                Console.WriteLine(vehicle.Id + " " + vehicle.VehicleType + " " + vehicle.Balance);
            }
            catch (ApiException api)
            {
                Console.WriteLine(api.StatusCode + " " + api.Content);
            }
            Console.ReadLine();

        }


        private static async Task ShowVechicles()
        {
            Console.WriteLine("Vehicles");
            string s = "vehicles";
            var vehicles = await Get<List<Vehicle>>.GetSimple(s);
            foreach (var item in vehicles)
            {
                Console.WriteLine(item.Id + " " + item.VehicleType + " " + item.Balance);
            }
            Console.ReadLine();
        }


        private static async Task ShowTransaction()
        {
            Console.WriteLine("Last transaction");
            string s = "transactions/last";
            try
            {
                var lastTransaction = await Get<TransactionInfo>.GetSimple(s);
                Console.WriteLine(lastTransaction.Id + " " + lastTransaction.Sum + " " + lastTransaction.time);
            }
            catch (ApiException api)
            {
                Console.WriteLine(api.StatusCode + " " + api.Content);
            }
            Console.ReadLine();
        }


        private static async Task ShowHistory()
        {
            Console.WriteLine("History");
            string s = "transactions/all";
            try
            {
                var history = await Get<string>.GetSimple(s);
                Console.WriteLine(history);
            }
            catch (ApiException api)
            {
                Console.WriteLine(api.StatusCode + " " + api.Content);
            }
            Console.ReadLine();
        }


        private async static Task AddVechicle()
        {
            Console.WriteLine("Add vehicle");
            try
            {
                Console.WriteLine("Please enter Id, VechicleType, Balance");
                string str = Console.ReadLine();
                VehicleType vehicleType;
                string[] mystring = str.Split(' ');

                switch (mystring[1])
                {
                    case "PassengerCar":
                        vehicleType = VehicleType.PassengerCar;
                        break;
                    case "Truck":
                        vehicleType = VehicleType.Truck;
                        break;
                    case "Bus":
                        vehicleType = VehicleType.Bus;
                        break;
                    case "Motorcycle":
                        vehicleType = VehicleType.Motorcycle;
                        break;
                    default:
                        throw new Exception();

                }
                Vehicle vehicle = new Vehicle(mystring[0], vehicleType, decimal.Parse(mystring[2]));

                var vv = await PostAsyncV(vehicle);
                Console.WriteLine(vv.Id + " " + vv.Balance + " " + vv.VehicleType);
            }
            catch (ApiException api)
            {
                Console.WriteLine(api.StatusCode + " " + api.Content);
            }
            Console.ReadLine();

        }

        private static async Task Capacity()
        {
            Console.WriteLine("Capacity");
            string s = "parking/capacity";
            var capacity = await Get<int>.GetSimple(s);
            Console.WriteLine(capacity);
            Console.ReadLine();
        }


        private static async Task FreePlaces()
        {
            Console.WriteLine("Free places");
            string s = "parking/freePlaces";
            var freePlaces = await Get<int>.GetSimple(s);
            Console.WriteLine(freePlaces);
            Console.ReadLine();
        }


        public class ApiException : Exception
        {
            public int StatusCode { get; set; }

            public string Content { get; set; }
        }

        class Get<T>
        {
            public static async Task<T> GetSimple(string s)
            {
                using (var client = new HttpClient())
                using (var request = new HttpRequestMessage(HttpMethod.Get, $"http://localhost:52475/api/{s}"))
                using (var response = await client.SendAsync(request))
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Console.WriteLine();

                    if (response.IsSuccessStatusCode == false)
                    {
                        //Console.WriteLine((int)response.StatusCode + " " + content);
                        throw new ApiException
                        {
                            StatusCode = (int)response.StatusCode,
                            Content = content
                        };
                    }
                    if(String.IsNullOrEmpty(content))
                    {
                        throw new Exception();
                    }
                    else
                    return JsonConvert.DeserializeObject<T>(content);
                }
            }
        }

    


        private static async Task<Vehicle> PostAsyncV(Vehicle vehicle)
        {
            using (var client = new HttpClient())
            using (var response = await client.PostAsJsonAsync("http://localhost:52475/api/vehicles", vehicle))
            {
                var content = await response.Content.ReadAsStringAsync();

                Console.WriteLine((int)response.StatusCode);
                if (response.IsSuccessStatusCode == false)
                {
                    Console.WriteLine((int)response.StatusCode + " " + content);
                    throw new ApiException
                    {
                        StatusCode = (int)response.StatusCode,
                        Content = content
                    };
                }

                return JsonConvert.DeserializeObject<Vehicle>(content);
            }
        }


        private static async Task<Vehicle> PutAsyncV(ToUp toUp)
        {
            using (var client = new HttpClient())
            using (var response = await client.PutAsJsonAsync("http://localhost:52475/api/transactions/topUpVehicle", toUp))
            {
                var content = await response.Content.ReadAsStringAsync();

                Console.WriteLine((int)response.StatusCode);
                if (response.IsSuccessStatusCode == false)
                {
                    Console.WriteLine((int)response.StatusCode + " " + content);
                    throw new ApiException
                    {
                        StatusCode = (int)response.StatusCode,
                        Content = content
                    };
                }

                return JsonConvert.DeserializeObject<Vehicle>(content);
            }
        }


        private static async void DeleteAsyncV(string id)
        {
            using (var client = new HttpClient())
            
            using (var response = await client.DeleteAsync($"http://localhost:52475/api/{id}"))
            {
                var content = await response.Content.ReadAsStringAsync();

                Console.WriteLine((int)response.StatusCode);
                if (response.IsSuccessStatusCode == false)
                {
                    Console.WriteLine((int)response.StatusCode + " " + content);
                    throw new ApiException
                    {
                        StatusCode = (int)response.StatusCode,
                        Content = content
                    };
                }


            }
        }



    

        private async static Task ShowBalance()
        {
            Console.WriteLine("Balance");
            string s = "parking/balance";
            var balance = await Get<decimal>.GetSimple(s);
            Console.WriteLine(balance);
            Console.ReadLine();

        }



        static void MainMenu()
        {

            while (true)
            {
                PopulateMenuItems();
                Console.Clear();

                Console.WriteLine("Main Menu");
                Console.WriteLine("---------\n");

                for (int i = 0; i < MenuItems.Count; i++)
                {
                    Console.WriteLine($"  {i + 1}. {MenuItems[i].Description}");
                }

                Console.Write($"Enter a choice (1 - {MenuItems.Count}): ");
                int userInput = 0;
                int.TryParse(Console.ReadLine(), out userInput);
                if (userInput == MenuItems.Count)
                    break;

                MenuItems[userInput - 1].Execute().GetAwaiter().GetResult();
            }

        }




        static void Main(string[] args)
        {
            try
            {                
                MainMenu();
            }
            finally
            {
                Console.WriteLine("The end");
            }

        }
    }
}
